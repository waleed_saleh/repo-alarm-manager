""" Load dependencies """

from __init__ import version  # pylint: disable=W0403
from troposphere import Ref, Join, Sub, Output, awslambda, iam, Export, GetAtt, events, Not, Equals, cloudwatch, If

import awacs.ec2
import awacs.s3
import awacs.awslambda
import awacs.logs
import awacs.sts
from awacs.aws import Allow, Statement, Policy, Principal

from stacker.blueprints.variables.types import CFNString, CFNNumber
from stacker.blueprints.base import Blueprint


class BlueprintClass(Blueprint):
    """Blueprint for creating lambda function."""

    VARIABLES = {
        'ApplicationName': {
            'type': CFNString,
            'description': 'Name of lambda',
            'default': 'ec2-nightly-rerun-alarm-manager'
        },
        'CustomerName': {
            'type': CFNString,
            'description': 'The Customer Name'
        },
        'EnvironmentName': {
            'type': CFNString,
            'description': 'The Environment Name',
            'default': 'common'
        },
        'LambdaHandler': {
            'type': CFNString,
            'description': 'Lambda entrypoint',
            'default': 'lambda.lambda_handler'
        },
        'LambdaDescription': {
            'type': CFNString,
            'description': '',
            'default': 'Nightly rerun of alarm manager creation of alarms for key: Product value: Kubernetes'
        },
        'FunctionMemorySize': {
            'type': CFNString,
            'description': 'How much memory do you want to allocate to this'
                           ' function?',
            'default': '256'
        },
        'FunctionRuntime': {
            'type': CFNString,
            'description': 'Which runtime should be used for the function?',
            'allowed_values': ['nodejs4.3', 'nodejs6.10', 'python3.7'],
            'default': 'python3.7'
        },
        'FunctionTimeout': {
            'type': CFNString,
            'description': 'How long should the function be allowed to run',
            'default': '300'
        },
        'CodeS3Bucket': {
            'type': CFNString,
            'description': 'CodeS3Bucket'
        },
        'CodeS3Key': {
            'type': CFNString,
            'description': 'CodeS3Key'
        },
        'BackupKey': {
            'type': CFNString,
            'default': 'Backup',
            'description': 'Optional. Override EBS Tag key for configuring '
                           'backup. Default is Backup.',
        },
        'RetentionKey': {
            'type': CFNString,
            'default': 'Retention',
            'description': 'Optional. Override EBS Tag key for configuring '
                           'retention. Default is Retention.',
        },
        'DefaultRetentionDays': {
            'type': CFNString,
            'default': '7',
            'description': 'Optional. Number of days to retain snapshots. '
                           'Default is 7 days.',
        },
        'TimeZone': {
            'type': CFNString,
            'default': 'US/Eastern',
            'description': 'Optional. Python formatted timezone.',
        },
        'AWSRegion': {
            'type': CFNString,
            'default': 'us-east-1',
            'description': 'Optional. AWS region where backup is taking place.',
        },
        'ExecutionRateValue': {
            'type': CFNNumber,
            'description': 'A positive number for '
                                              'how often it runs',
            'default': '1',
            'min_value': '1'
        },
        'ExecutionRateUnit': {
            'type': CFNString,
            'description': 'A unit of time for execution',
            'default': 'day',
            'allowed_values': ['minute', 'minutes', 'hour', 'hours', 'day', 'days']
        },
        'AlertTopicArn': {
            'type': CFNString,
            'default': '',
            'description': 'Arn of the SNS alert topic to send alarms to',
        },
        'AlertInsuffTopicArn': {
            'type': CFNString,
            'default': '',
            'description': 'Arn of the SNS topic to send alarms to when '
                           'insufficient',
        },
        'AlarmThreshold': {
            'type': CFNString,
            'default': '0',
            'description': 'Expression for the rule to be scheduled',
        },
        'AlarmPeriod': {
            'type': CFNString,
            'default': '60',
            'description': 'How many minutes in the sample period',
        },
        'AlarmPeriodCount': {
            'type': CFNString,
            'default': '5',
            'description': 'How many periods in the sample',
        }
    }

    def add_conditions(self):
        """Set up template conditions."""
        template = self.template
        variables = self.get_variables()

        template.add_condition(
            'HasSNSAlertTopic',
            Not(Equals(variables['AlertTopicArn'].ref, ''))
        )
        template.add_condition(
            'HasSNSInsufficientTopic',
            Not(Equals(variables['AlertInsuffTopicArn'].ref, ''))
        )

    def create_resources(self):
        """Create the resources."""
        template = self.template
        variables = self.get_variables()

        lambda_iam_role = template.add_resource(
            iam.Role(
                'LambdaRole',
                RoleName=Join('-',
                              [variables['CustomerName'].ref,
                               variables['ApplicationName'].ref,
                               variables['EnvironmentName'].ref,
                               'lambda-role']),
                AssumeRolePolicyDocument=Policy(
                    Version='2012-10-17',
                    Statement=[
                        Statement(
                            Effect=Allow,
                            Action=[awacs.sts.AssumeRole],
                            Principal=Principal('Service',
                                                ['lambda.amazonaws.com'])
                        )
                    ]
                ),
                Path='/service-role/',
                ManagedPolicyArns=[
                    'arn:aws:iam::aws:policy/'
                    'AWSXrayWriteOnlyAccess',
                    'arn:aws:iam::aws:policy/service-role/'
                    'AWSLambdaBasicExecutionRole',
                    'arn:aws:iam::aws:policy/service-role/'
                    'AWSLambdaVPCAccessExecutionRole'
                ],
                Policies=[
                    iam.Policy(
                        PolicyName=Join('-',
                                        [variables['CustomerName'].ref,
                                         variables['ApplicationName'].ref,
                                         variables['EnvironmentName'].ref,
                                         'lambda-policy']),
                        PolicyDocument=Policy(
                            Version='2012-10-17',
                            Statement=[
                                Statement(
                                    Action=[
                                        awacs.logs.CreateLogGroup,
                                        awacs.logs.CreateLogStream,
                                        awacs.logs.PutLogEvents
                                    ],
                                    Effect=Allow,
                                    Resource=['arn:aws:logs:*:*:*'],
                                    Sid='WriteLogs'
                                ),
                                Statement(
                                    Action=[
                                        awacs.ec2.DescribeInstances,
                                    ],
                                    Effect=Allow,
                                    Resource=['*'],
                                    Sid='EC2Access'
                                ),
                                Statement(
                                    Action=[
                                        awacs.awslambda.InvokeFunction,
                                    ],
                                    Effect=Allow,
                                    Resource=['*'],
                                    Sid='InvokeFunction'
                                ),
                                Statement(
                                    Action=[
                                        awacs.cloudwatch.ListMetrics,
                                    ],
                                    Effect=Allow,
                                    Resource=['*'],
                                    Sid='ListMetrics'
                                )
                            ]
                        )
                    )
                ]
            )
        )

        env_vars = awslambda.Environment(
            Variables={
                'BACKUP_KEY': variables['BackupKey'].ref,
                'RETENTION_KEY': variables['RetentionKey'].ref,
                'DEFAULT_RETENTION_DAYS': variables['DefaultRetentionDays'].ref,
                'TIME_ZONE': variables['TimeZone'].ref
            }
        )

        lambda_function = template.add_resource(
            awslambda.Function(
                'LambdaFunction',
                Description=variables['LambdaDescription'].ref,
                Code=awslambda.Code(
                    S3Bucket=variables['CodeS3Bucket'].ref,
                    S3Key=variables['CodeS3Key'].ref
                ),
                Environment=env_vars,
                Handler=variables['LambdaHandler'].ref,
                Role=GetAtt(lambda_iam_role, 'Arn'),
                Runtime=variables['FunctionRuntime'].ref,
                Timeout=variables['FunctionTimeout'].ref,
                MemorySize=variables['FunctionMemorySize'].ref,
                FunctionName=Join('-',
                                  [variables['CustomerName'].ref,
                                   variables['ApplicationName'].ref,
                                   'lambda',
                                   variables['EnvironmentName'].ref]),
                # TracingConfig='Active'
            )
        )

        scheduledevent = template.add_resource(
            events.Rule(
                'CloudwatchScheduledEvent',
                Description='An event to schedule the ebs snapshot manger lambda',
                ScheduleExpression=Sub('rate(${ExecutionRateValue} '
                                       '${ExecutionRateUnit})'),
                State='ENABLED',
                Targets=[events.Target(
                    Arn=GetAtt(lambda_function, 'Arn'),
                    Id='TargetFunctionV1'
                )]
            )
        )


        alarm = template.add_resource(  # pylint: disable=W0612
            cloudwatch.Alarm(
                'LambdaErrorAlarm',
                AlarmName=Join('-', [Ref(lambda_function),
                                     'lambda-exec-err']),
                AlarmDescription='Lambda Execution Errors',
                Namespace='AWS/Lambda',
                Statistic='Minimum',
                Period=variables['AlarmPeriod'].ref,
                EvaluationPeriods=variables['AlarmPeriodCount'].ref,
                Threshold=variables['AlarmThreshold'].ref,
                AlarmActions=If('HasSNSAlertTopic',
                                [variables['AlertTopicArn'].ref],
                                [Ref('AWS::NoValue')]),
                OKActions=If('HasSNSAlertTopic',
                             [variables['AlertTopicArn'].ref],
                             [Ref('AWS::NoValue')]),
                InsufficientDataActions=If('HasSNSAlertTopic',
                                           [variables['AlertTopicArn'].ref],
                                           [Ref('AWS::NoValue')]),
                ComparisonOperator='GreaterThanThreshold',
                Dimensions=[cloudwatch.MetricDimension(
                    Name='FunctionName',
                    Value=lambda_function.title
                )],
                MetricName='Errors',
                TreatMissingData='notBreaching'
            )
        )

        template.add_resource(
            awslambda.Permission(
                'LambdaInvokePermission',
                FunctionName=Ref(lambda_function),
                Action='lambda:InvokeFunction',
                Principal='events.amazonaws.com',
                SourceArn=GetAtt(scheduledevent, 'Arn')
            )
        )

        template.add_output(
            Output(
                lambda_iam_role.title,
                Description='Lambda Role',
                Export=Export(Sub('${AWS::StackName}-%s' % lambda_iam_role.title)),  # nopep8 pylint: disable=C0301
                Value=Ref(lambda_iam_role)
            )
        )
        template.add_output(
            Output(
                lambda_function.title,
                Description='Lambda Function',
                Export=Export(Sub('${AWS::StackName}-%s' % lambda_function.title)),  # nopep8 pylint: disable=C0301
                Value=GetAtt(lambda_function, 'Arn')
            )
        )
        template.add_output(
            Output(
                lambda_function.title + 'Name',
                Description='Lambda Function Name',
                Export=Export(Sub('${AWS::StackName}-%sName' % lambda_function.title)),  # nopep8 pylint: disable=C0301
                Value=Ref(lambda_function)
            )
        )

    def create_template(self):
        self.template.add_version('2010-09-09')
        self.template.add_description('EBS Snapshot Manager'
                                      ' - {0}'.format(version()))
        self.add_conditions()
        self.create_resources()


# Helper section to enable easy blueprint -> template generation
# (just run `python <thisfile>` to output the json)
if __name__ == "__main__":
    from stacker.context import Context
    from stacker.variables import Variable

    BLUEPRINT = BlueprintClass("test",
                               Context({"namespace": "test"}),
                               None)
    # Check the blueprint for CFN parameters in its variables, and define bogus
    # values for those parameters so the template can be generated.
    # Easiest check to find variables that are CFN parameters (and not native
    # python types) seems to be looking for the 'parameter_type' attribute
    TEST_VARIABLES = []
    for key, value in BLUEPRINT.defined_variables().iteritems():
        if hasattr(value['type'], 'parameter_type'):
            TEST_VARIABLES.append(Variable(key, 'go_sturdy'))

    BLUEPRINT.resolve_variables(TEST_VARIABLES)
    print BLUEPRINT.render_template()[1]
