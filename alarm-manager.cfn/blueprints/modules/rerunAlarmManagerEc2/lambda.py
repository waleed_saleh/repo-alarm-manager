"""Retroactively adds alarms to existing instances using alarm-manager"""
# TODO Add check to lambda function name to validate it exists before continuing.
import argparse
import logging
import json
import copy
import os
import boto3
import botocore
from botocore.exceptions import ClientError
from collections import defaultdict

log = logging.getLogger(__name__)

NAMESPACE = 'CWAgent' # namespace of all metrics
METRIC_CHECK = [
    {  # windows
        'MetricName': 'LogicalDisk % Free Space',
        'Dimensions': [
            {'Name': 'instance'}
        ]
    },
    {  # linux
        'MetricName': 'disk_used_percent',
        'Dimensions': [
            {'Name': 'device'}
        ]
    },
]
METRIC_NAMES = [ # list of metric names
    'disk_used_percent',
    'LogicalDisk % Free Space',
]
EXCLUDED_PATH_PREFIXES = (
    'loop',
    '//'
)
FILTER_LIST = []


def _filter_inst_data(inst):
    """Gets rid of data we dont need to pulls tags to the top level
    """
    data = {
        'instanceId': inst['InstanceId']
    }
    data.update({
        t['Key'].replace(' ', '_'): t['Value'].replace(' ', '_') for t in inst['Tags']
    })
    data['platform'] = inst.get('Platform', 'Linux') # placed after tags incase used as a tag
    data['_alarm_name'] = inst['InstanceId']
    return data


def get_instances(session, tags):
    """
    Collects instances based on a tag key/value pair passed as an argument
        multiple values can be specified but, filter condition is OR

    Example Argument:
        tagFilter = {
            'tag-keys': [
                'backup',
                'Backup'
            ],
            'tag-values': [
                'True',
                'true'
            ]
        }

    """
    filters = [
        {
            'Name': 'instance-state-name', # only get existing instances
            'Values': [
                # 'pending', # if deployed, this would be repetitive
                'running',
                'shutting-down',
                'stopping',
                'stopped'
            ]
        }
    ]

    if tags['tag-keys'] is not None:
        filters.append({
            'Name': 'tag-key',
            'Values': tags['tag-keys']
        })
    if tags['tag-values'] is not None:
        filters.append({
            'Name': 'tag-value',
            'Values': tags['tag-values']
        })
    response = session.client('ec2').describe_instances(
        Filters=filters
    )

    try:
        return sum([
            [_filter_inst_data(i) for i in r['Instances']]
            for r in response['Reservations']
        ], [])  # instances as a list
    except IndexError:  # no instances were returned
        print('no instances')
        log.error('No instances were found for given paramaters')
        return None


def check_metrics(session, namespace, name, dims=None):
    """Gets all metrics based on arguments and returns a dict"""
    metrics = []
    paginator = session.client('cloudwatch').get_paginator('list_metrics')
    props = {}
    if namespace is not None:
        props['Namespace'] = namespace
    if name is not None:
        props['MetricName'] = name
    if dims is not None:
        props['Dimensions'] = dims

    response = paginator.paginate(
        **props
    )
    for page in response:
        if dims is not None:
            for metric in page['Metrics']:
                if len(dims) == len(metric['Dimensions']):
                    metrics.append(metric)
        else:
            metrics += page['Metrics']
    return metrics


def get_dimension(metric, dimension, default=None):
    """Pass in a metric and dimension name. Returns dimension value."""
    return {
        m['Name']: m['Value'] for m in metric['Dimensions'] if m['Name'] == dimension
    }.get(dimension, default)


def _format_payload(mode, payload):
    if mode == 'delete':
        return [
            i['instanceId'] for i in payload
        ]
    return payload


def _choose_name(mode):
    if mode == 'create':
        return 'RunInstances'
    if mode == 'delete':
        return 'TerminateInstances'


def invoke_lambda(session, name, payload):
    response = session.client('lambda').invoke(
        FunctionName=name,
        InvocationType='Event', # send and continue
        Payload=json.dumps(payload)
    )
    return response


def create_disk_alarms(session, instanceIds, function_name):
    result = {
        'type': 'disk-alarms', # TODO fix this
        'name': 'CreateDiskAlarms',
        'region': session.region_name
    }
    metric_results = defaultdict(list)

    for inst in instanceIds:
        for metric in copy.deepcopy(METRIC_CHECK):
            metric['Dimensions'].append({
                'Name': 'InstanceId',
                'Value': inst
            })
            for m in check_metrics(
                session,
                NAMESPACE,
                metric['MetricName'],
                metric['Dimensions']
            ):
                path = ''
                if metric['MetricName'] == 'disk_used_percent':
                    path = get_dimension(m, 'device', None)
                else:
                    path = get_dimension(m, 'instance', None)
                if path is not None and not path.startswith(EXCLUDED_PATH_PREFIXES):
                    metric_results[inst].append(path)

    result['data'] = metric_results
    return session.client('lambda').invoke(
        FunctionName=function_name,
        InvocationType='Event', # send and continue
        Payload=json.dumps(result)
    )


def lambda_handler(event, context):
    session = boto3.session.Session(region_name=os.environ["AWS_DEFAULT_REGION"])
    # cwclient = boto3.client('cloudwatch', region_name=args['region'])
    tag_filter = {
        'tag-keys': ["Product"],
        'tag-values': ["Kubernetes"]
    }
    instances = get_instances(session, tag_filter)
    for i in instances:
        print('{}    Product: Kubernetes'.format(
            i['instanceId']))
    print('Found {} instance(s)'.format(len(instances)))
    payload = {
        'type': 'retro-alarm',
        'name': _choose_name("create"),
        'region': session.region_name,
        'source': 'ec2',
        'instances': _format_payload("create", instances)
    }
    response = invoke_lambda(session, "alarm-manager-ec2", payload)
    create_disk_alarms(session, [i['instanceId'] for i in instances], "alarm-manager-ec2")
