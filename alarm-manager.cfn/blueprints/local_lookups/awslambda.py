#!/usr/bin/env python
"""Helper classes for Stacker AWS Lambda hook."""


def bucket(value, context, **kwargs):  # pylint: disable=unused-argument
    """Extract the S3 bucket for a given package in hook_data.

    Format of value:
        <hook_name>::<key>
    """
    try:
        hook_name, hook_key = value.split("::")
    except ValueError:
        raise ValueError("Invalid value for hook_data: %s. Must be in "
                         "<hook_name>::<key> format." % value)

    code = context.hook_data[hook_name][hook_key]
    return code.to_dict()['S3Bucket']


def key(value, context, **kwargs):  # pylint: disable=unused-argument
    """Extract the S3 key for a given package in hook_data.

    Format of value:
        <hook_name>::<key>
    """
    try:
        hook_name, hook_key = value.split("::")
    except ValueError:
        raise ValueError("Invalid value for hook_data: %s. Must be in "
                         "<hook_name>::<key> format." % value)

    code = context.hook_data[hook_name][hook_key]
    return code.to_dict()['S3Key']
